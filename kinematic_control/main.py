from myfunctions import *


# connect to NAO
motion_proxy = ALProxy("ALMotion", IP, PORT)
posture_proxy = ALProxy("ALRobotPosture", IP, PORT)

posture_proxy.goToPosture("Crouch", 0.5)

# move head
t_h0 = np.array([[0], [0], [0]])
theta_h = np.array([0, 20, 0]) * almath.TO_RAD
T_h0 = pos2tf(t_h0, theta_h)
set_tf(motion_proxy, HEAD, flat_tf(T_h0))

# open hand
motion_proxy.setAngles(R_HAND, HAND_OPEN, 0.2)

# control arm
K = 0.5
pd = INIT_END_EFFECTOR_POS.flatten()
pd = np.concatenate((pd, INIT_END_EFFECTOR_ROT))
pd = list(pd)
pd = np.array(pd)

# simple command
# motion_proxy.setPosition(R_ARM, motion.FRAME_TORSO, list(pd), 0.5, AXIS_MASK_ALL)

p = motion_proxy.getPosition(R_ARM, motion.FRAME_TORSO, False)
p = np.array(p)

err = pd - p
while np.max(np.abs(err)) > 0.02:
    p += K * err
    motion_proxy.setPosition(R_ARM, motion.FRAME_TORSO, list(p), 0.5, AXIS_MASK_ALL)

    p = motion_proxy.getPosition(R_ARM, motion.FRAME_TORSO, False)
    p = np.array(p)
    err = pd - p
    print np.max(np.abs(err))

print pd
print p
print np.max(np.abs(err))







