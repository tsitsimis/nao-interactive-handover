from myfunctions import *


# connect to NAO
motion_proxy = ALProxy("ALMotion", IP, PORT)
posture_proxy = ALProxy("ALRobotPosture", IP, PORT)

posture_proxy.goToPosture("Crouch", 0.5)

# move head
t_h0 = np.array([[0], [0], [0]])
theta_h = np.array([0, 20, 0]) * almath.TO_RAD
T_h0 = pos2tf(t_h0, theta_h)
set_tf(motion_proxy, HEAD, flat_tf(T_h0))

# open hand
motion_proxy.setAngles(R_HAND, HAND_OPEN, 0.2)

# control transform
t_e0 = INIT_END_EFFECTOR_POS
theta_e = INIT_END_EFFECTOR_ROT
T_e0 = pos2tf(t_e0, theta_e)
Td = flat_tf(T_e0)
Td = np.array(Td)

transform_controller(motion_proxy, R_ARM, Td)



