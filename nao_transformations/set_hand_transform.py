from myfunctions import *


# connect to NAO
IP = "172.17.0.1"
# IP = "192.168.0.108"
PORT = 9559
motionProxy = ALProxy("ALMotion", IP, PORT)
postureProxy = ALProxy("ALRobotPosture", IP, PORT)

# init
chainName = "RArm"
space = motion.FRAME_TORSO
useSensor = False
nao_stiffness(IP, PORT, 1.0)
nao_posture(IP, PORT, "Crouch")

# get position
current = motionProxy.getPosition(chainName, space, useSensor)
print current

# move RArm
init_arm(IP, PORT)
time.sleep(5)

# get position
current = motionProxy.getPosition(chainName, space, useSensor)
print current

# move RArm2
init_joints(IP, PORT, 0.2)
time.sleep(5)

# get position
current = motionProxy.getPosition(chainName, space, useSensor)
print current



