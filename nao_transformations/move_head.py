from myfunctions import *


# connect to NAO
# IP = "172.17.0.1"
IP = "192.168.0.108"
PORT = 9559
motionProxy = ALProxy("ALMotion", IP, PORT)
postureProxy = ALProxy("ALRobotPosture", IP, PORT)

# initialize joints
speed = 0.2
nao_stiffness(IP, PORT, 1.0)
nao_posture(IP, PORT, "StandInit")


chainName = "Head"
space = motion.FRAME_TORSO
useSensor = False

# get current position
current = motionProxy.getPosition(chainName, space, useSensor)

target = [
    current[0] + 0.0,
    current[1] + 0.0,
    current[2] + 0.0,
    current[3] + 0.0,
    current[4] + 0.0,
    current[5] + almath.PI_2
]

fractionMaxSpeed = 0.5
axisMask = AXIS_MASK_X + AXIS_MASK_Y + AXIS_MASK_Z + \
           AXIS_MASK_WX + AXIS_MASK_WY + AXIS_MASK_WZ
motionProxy.setPosition(chainName, space, target, fractionMaxSpeed, axisMask)














