from myfunctions import *


# connect to NAO
# IP = "172.17.0.1"
IP = "192.168.0.108"
PORT = 9559
motion_proxy = ALProxy("ALMotion", IP, PORT)
posture_proxy = ALProxy("ALRobotPosture", IP, PORT)
last_row = np.array([[0, 0, 0, 1]])

# stand
posture_proxy.goToPosture("StandInit", 0.5)

# INIT
# 1. set initial head transform
t_h0 = np.array([[0], [0], [0]])
theta_h0 = np.array([0, 20, 0])
r_h0 = euler2matrix(theta_h0 * almath.TO_RAD)
tf_h0 = np.concatenate((r_h0, t_h0), axis=1)
tf_h0 = np.concatenate((tf_h0, last_row), axis=0)
tf_h0_f = list(tf_h0.flatten()[0:12])
set_tf(IP, PORT, "Head", tf_h0_f)

# 2. set initial end effector transform
t_e0 = np.array([[0.18109862506389618], [-0.053951337933540344], [0.12278126180171967]])
theta_e0 = np.array([1.0500797033309937, -0.433413565158844, 0.31683531403541565])
r_e0 = euler2matrix(theta_e0)
tf_e0 = np.concatenate((r_e0, t_e0), axis=1)
tf_e0 = np.concatenate((tf_e0, last_row), axis=0)
tf_e0_f = list(tf_e0.flatten()[0:12])
set_tf(IP, PORT, "RArm", tf_e0_f)
print tf_e0_f

# 3. open gripper
motion_proxy.setAngles(RHandName, 1.0, 0.2)
time.sleep(5)

# MOVE
# 1. move head
theta_h1 = np.array([0, 10, -10]) * almath.TO_RAD
r_h1 = euler2matrix(theta_h1)
tf_h1 = np.concatenate((r_h1, t_h0), axis=1)
tf_h1 = np.concatenate((tf_h1, last_row), axis=0)
tf_h1_f = list(tf_h1.flatten()[0:12])
set_tf(IP, PORT, "Head", tf_h1_f)

# 2. move end effector accordingly
tf_e1 = np.dot(np.dot(tf_h1, np.linalg.inv(tf_h0)), tf_e0)
tf_e1_f = list(tf_e1.flatten()[0:12])
set_tf(IP, PORT, "RArm", tf_e1_f)
print tf_e1_f




