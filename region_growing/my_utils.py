import cv2
import numpy as np
from matplotlib import pyplot as plt
from sklearn.mixture import GaussianMixture
from scipy.stats import multivariate_normal
from imutils import build_montages
from PolygonDrawer import PolygonDrawer


def region_growing(img, seed):
    # Parameters for region growing
    neighbors = [(-1, 0), (1, 0), (0, -1), (0, 1)]
    region_threshold = 0.2
    region_size = 1
    intensity_difference = 0
    neighbor_points_list = []
    neighbor_intensity_list = []

    # Mean of the segmented region
    region_mean = img[seed]

    # Input image parameters
    height, width = img.shape
    image_size = height * width

    # Initialize segmented output image
    segmented_img = np.zeros((height, width, 1), np.uint8)

    # Region growing until intensity difference becomes greater than certain threshold
    while (intensity_difference < region_threshold) & (region_size < image_size):
        # Loop through neighbor pixels
        for i in range(4):
            # Compute the neighbor pixel position
            x_new = seed[0] + neighbors[i][0]
            y_new = seed[1] + neighbors[i][1]

            # Boundary Condition - check if the coordinates are inside the image
            check_inside = (x_new >= 0) & (y_new >= 0) & (x_new < height) & (y_new < width)

            # Add neighbor if inside and not already in segmented_img
            if check_inside:
                if segmented_img[x_new, y_new] == 0:
                    neighbor_points_list.append([x_new, y_new])
                    neighbor_intensity_list.append(img[x_new, y_new])
                    segmented_img[x_new, y_new] = 255

        # Add pixel with intensity nearest to the mean to the region
        distance = abs(neighbor_intensity_list - region_mean)
        pixel_distance = min(distance)
        index = np.where(distance == pixel_distance)[0][0]
        segmented_img[seed[0], seed[1]] = 255
        region_size += 1

        # New region mean
        region_mean = (region_mean * region_size + neighbor_intensity_list[index]) / (region_size + 1)

        # Update the seed value
        seed = neighbor_points_list[index]
        # Remove the value from the neighborhood lists
        neighbor_intensity_list[index] = neighbor_intensity_list[-1]
        neighbor_points_list[index] = neighbor_points_list[-1]

    return segmented_img


def eval_sample(img, mean, cov):
    proba_img = 0.5 * multivariate_normal.pdf(img, mean[0], cov[0]) + \
                0.5 * multivariate_normal.pdf(img, mean[1], cov[1])
    proba_img /= np.max(proba_img)
    proba_img *= 255
    return proba_img


def get_contour(proba_img, t=10):
    proba_img = proba_img
    thres = cv2.inRange(proba_img, t, 255)

    kernel = np.ones((3, 3), dtype=np.uint8)
    opening = cv2.morphologyEx(thres, cv2.MORPH_OPEN, kernel)

    kernel = np.ones((3, 3), dtype=np.uint8)
    closing = cv2.morphologyEx(opening, cv2.MORPH_CLOSE, kernel)

    mask = closing
    # mask = np.dstack([mask] * 3)
    return mask


def collect_samples(n, save=False, filename="samples.txt"):
    color_points = np.zeros((0, 3))

    for i in range(n):
        # take sample image
        camera = cv2.VideoCapture(0)
        while True:
            # grab the current frame
            (grabbed, frame) = camera.read()

            # show the frame to our screen
            cv2.imshow("Frame", frame)

            key = cv2.waitKey(1) & 0xFF
            if key == ord("o"):
                img = frame
                break

        camera.release()
        cv2.destroyAllWindows()

        # select color region
        pd = PolygonDrawer(img.copy())
        mask = pd.get_mask()

        # get pixel values
        loc = np.where(mask != 0)
        cp = img[loc[0], loc[1], :]
        color_points = np.concatenate((color_points, cp), axis=0)
        if save:
            np.savetxt(filename, color_points)

    return color_points


def load_samples(filename):
    color_points = np.loadtxt(filename)
    return color_points


def fit_model(color_points):
    model = GaussianMixture(2).fit(color_points)
    mean = model.means_
    cov = model.covariances_
    return mean, cov


def show_results(image_list):
    n_images = len(image_list)
    for i in range(n_images):
        if len(np.shape(image_list[i])) < 3:
            image_list[i] = np.dstack([image_list[i]]*3)

    cv2.namedWindow("montage", cv2.WINDOW_NORMAL)
    montages = build_montages(image_list,  (640/2, 480/2), (n_images, 1))
    for montage in montages:
        cv2.imshow("montage", montage)


