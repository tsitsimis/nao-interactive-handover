from myfunctions import *

# Connect to NAO
IP = "192.168.0.108"
PORT = 9559

videoProxy = ALProxy("ALVideoDevice", IP, PORT)
motionProxy = ALProxy("ALMotion", IP, PORT)
postureProxy = ALProxy("ALRobotPosture", IP, PORT)

# initialize joints
speed = 0.2
# nao_stiffness(IP, PORT, 1.0)
# init_joints(motionProxy, speed)

# initialize camera
subscriber = init_cam(videoProxy)

# save images to this directory
image_prefix = "chess_"
my_dir = "./nao_chess2/"
cnt = 0

while True:
    frame = cam2numpy(videoProxy, subscriber)   # NAO

    # show frame
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    cv2.imshow("Frame", frame)

    gray = cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)
    ret, corners = cv2.findChessboardCorners(gray, (7, 6), None)

    key = cv2.waitKey(1) & 0xFF
    if key == ord("q"):
        break
    elif key == ord("1"):
        if ret:
            img = Image.fromarray(gray)
            img.save(my_dir + image_prefix + str(cnt) + ".jpg")
            cnt += 1
        else:
            print "NO"

videoProxy.unsubscribe(subscriber)
