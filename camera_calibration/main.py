from myfunctions import *


# connect to NAO
IP = "192.168.0.108"
PORT = 9559
ttsProxy = ALProxy("ALTextToSpeech", IP, PORT)
motionProxy = ALProxy("ALMotion", IP, PORT)
videoProxy = ALProxy("ALVideoDevice", IP, PORT)
photoCaptureProxy = ALProxy("ALPhotoCapture", IP, PORT)
postureProxy = ALProxy("ALRobotPosture", IP, PORT)
audioProxy = ALProxy("ALAudioPlayer", IP, PORT)

# initialize camera
subscriber = init_cam(videoProxy)
red = (255, 0, 0)
blue = (0, 0, 255)

# initialize joints
speed = 0.2
# nao_stiffness(IP, PORT, 1.0)
# nao_posture(IP, PORT, "Crouch")

P = np.array([10, 0, 30])
x = np.dot(cam_internal_params3(), P) / P[2]
print x

window_name = "Frame"
cv2.namedWindow(window_name, 1)

while True:
    frame = cam2numpy(videoProxy, subscriber)
    cv2.setMouseCallback(window_name, click_callback, None)

    cv2.circle(frame, (640 / 2, 480 / 2), 4, (255, 0, 0), 4)

    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)  # BGR 2 RGB
    cv2.imshow(window_name, frame)

    key = cv2.waitKey(1) & 0xFF  # press q to exit
    if key == ord("q"):
        break

videoProxy.unsubscribe(subscriber)


