import numpy as np
import cv2
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import os
from naoqi import ALProxy
import vision_definitions as vd
import Image
from sklearn import svm, metrics, model_selection, neighbors, tree
from scipy.stats import multivariate_normal
from scipy import interpolate
import almath
import time
import Queue

# joint names
headYawName = "HeadYaw"
headPitchName = "HeadPitch"
RShoulderRollName = "RShoulderRoll"
RShoulderPitchName = "RShoulderPitch"
RElbowRollName = "RElbowRoll"
RElbowYawName = "RElbowYaw"
RWristYawName = "RWristYaw"
RHandName = "RHand"


def init_joints(motion_proxy, speed):
    motion_proxy.setStiffnesses(["RArm", "Head"], 1.0)
    motion_proxy.setAngles(RElbowRollName, 39.3 * almath.TO_RAD, speed)
    motion_proxy.setAngles(RShoulderRollName, 9.8 * almath.TO_RAD, speed)
    motion_proxy.setAngles(RShoulderPitchName, 10 * almath.TO_RAD, speed)
    motion_proxy.setAngles(RElbowYawName, 62.8 * almath.TO_RAD, speed)
    motion_proxy.setAngles(RWristYawName, 0.0 * almath.TO_RAD, speed)
    motion_proxy.setAngles(RHandName, 1.0, speed)
    motion_proxy.setAngles(headPitchName, 20 * almath.TO_RAD, speed)


def init_cam(video_proxy):
    cam_bottom = 0
    fps = 12
    try:
        video_proxy.unsubscribe("demo")
    except:
        pass
    subscriber = video_proxy.subscribeCamera("demo", cam_bottom, vd.kVGA, vd.kRGBColorSpace, fps)
    return subscriber


def cam2numpy(video_proxy, subscriber):
    # read image from NAO camera
    nao_image = video_proxy.getImageRemote(subscriber)
    image_width = nao_image[0]
    image_height = nao_image[1]
    array = nao_image[6]
    # convert NAO's image to numpy array
    frame = Image.frombytes("RGB", (image_width, image_height), array)
    frame = np.array(frame)
    return frame


def color_range(color):
    if color == "green":
        lower = (42, 42, 56)
        upper = (83, 255, 255)
    elif color == "cyan":
        lower = (0, 129, 95)
        upper = (255, 255, 255)
    elif color == "red":
        lower = (0, 146, 118)
        upper = (255, 255, 255)
    elif color == "marker":
        lower = (0, 92, 171)
        upper = (78, 255, 255)

    return lower, upper


def get_object_contour(img, color):
    hsv = cv2.cvtColor(img, cv2.COLOR_RGB2HSV)  # convert to HSV color space

    lower, upper = color_range(color)  # threshold min & max values
    thresh = cv2.inRange(hsv, lower, upper)  # apply threshold in HSV

    # morphological filtering
    kernel = np.ones((3, 3), np.uint8)
    opening = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel, iterations=2)
    closing = cv2.morphologyEx(opening, cv2.MORPH_CLOSE, kernel, iterations=2)

    # contours
    thresh = closing
    contours = cv2.findContours(thresh.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[-2]
    if len(contours) > 0:
        c = max(contours, key=cv2.contourArea)
        return True, c, thresh
    return False, [], thresh


def contour2box(c):
    rect = cv2.minAreaRect(c)
    box = cv2.boxPoints(rect)
    box = box.astype(int)
    return box


def contour_features(contour):
    # fit rectangle
    ((_, _), (width, height), angle) = cv2.minAreaRect(contour)  # rectangle

    # centroid
    mom = cv2.moments(contour)
    cx = int(mom['m10'] / mom['m00'])
    cy = int(mom['m01'] / mom['m00'])

    # orientation
    if width > height:
        orientation = np.abs(angle)
    else:
        orientation = (90 - np.abs(angle))

    # rectangle corners
    nw_x = cx - width / 2
    nw_y = cy - height / 2

    ne_x = cx + width / 2
    ne_y = cy - height / 2

    sw_x = cx - width / 2
    sw_y = cy + height / 2

    se_x = cx + width / 2
    se_y = cy + height / 2

    # features vector
    features = [cx, cy, orientation, width, height]
    # features = [cx, cy, nw_x, nw_y, ne_x, ne_y, sw_x, sw_y, se_x, se_y, orientation, width, height]
    return features


def nao_stiffness(ip, port, v):
    motion_proxy = ALProxy("ALMotion", ip, port)
    motion_proxy.setStiffnesses(["Body"], v)


def nao_posture(ip, port, posture, speed=0.5):
    posture_proxy = ALProxy("ALRobotPosture", ip, port)
    posture_proxy.goToPosture(posture, speed)  # blocking call


def head_follow_marker(ip, port, x0, y0, cx, cy):
    motion_proxy = ALProxy("ALMotion", ip, port)
    head_speed = 0.03

    delta_x = (x0 - cx) / 640  # error
    if np.abs(delta_x) >= 0.0:
        K = 10
        delta_yaw = K * delta_x
        head_yaw = motion_proxy.getAngles(headYawName, False)[0] * almath.TO_DEG
        head_yaw += delta_yaw

        head_yaw = np.min([head_yaw, 70])
        head_yaw = np.max([head_yaw, -70])

        head_yaw = head_yaw * almath.TO_RAD
        motion_proxy.setAngles(headYawName, head_yaw, head_speed)

    # dy = (imageHeight / 2.0 - cy) / (imageHeight / 2)
    # if np.abs(dy) >= 0.2:
    #     K = 10
    #     deltaPicth = K * dy
    #     headPitch = motion_proxy.getAngles(headPitchName, False)[0] * almath.TO_DEG
    #     headPitch -= deltaPicth
    #
    #     headPitch = np.min([headPitch, 20])
    #     headPitch = np.max([headPitch, -30])
    #
    #     headPitchRad = headPitch * almath.TO_RAD
    #     motion_proxy.setAngles(headPitchName, headPitchRad, head_speed)


def cam_internal_params():
    K = np.array([[378.64049, 0, 159.5],
                  [0, 380.07872, 119.5],
                  [0, 0, 1]])
    return K


def cam_internal_params2():
    K = np.array([[714.44277902, 0, 409.47819075],
                  [0, 704.97373151, 228.4659737],
                  [0, 0, 1]])
    return K


def cam_internal_params3():  # matlab
    K = np.array([[740.0286, 0, 338.5911],
                  [0, 738.8731, 233.1552],
                  [0, 0, 1]])
    return K


def click_callback(event, x, y, flags, userdata):
    if event == cv2.EVENT_LBUTTONDOWN:
        print "(" + str(x) + ", " + str(y) + ")"
        frame = userdata
        cv2.putText(frame, "(" + str(x) + ", " + str(y) + ")",
                        (x, y), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (255, 0, 0))







