import numpy as np
import imutils
import cv2
import matplotlib.pyplot as plt
from naoqi import ALProxy
import vision_definitions as vd
import Image


# connect to NAO
# IP = "192.168.0.108"
# PORT = 9559
# ttsProxy = ALProxy("ALTextToSpeech", IP, PORT)
# motionProxy = ALProxy("ALMotion", IP, PORT)
# videoProxy = ALProxy("ALVideoDevice", IP, PORT)
# photoCaptureProxy = ALProxy("ALPhotoCapture", IP, PORT)
# postureProxy = ALProxy("ALRobotPosture", IP, PORT)


def init_cam(video_proxy):
    cam_bottom = 0
    fps = 12
    try:
        video_proxy.unsubscribe("demo")
    except:
        pass
    subscriber = video_proxy.subscribeCamera("demo", cam_bottom, vd.kVGA, vd.kRGBColorSpace, fps)
    return subscriber


def cam2numpy(video_proxy, subscriber):
    # read image from NAO camera
    nao_image = video_proxy.getImageRemote(subscriber)
    image_width = nao_image[0]
    image_height = nao_image[1]
    array = nao_image[6]
    # convert NAO's image to numpy array
    frame = Image.frombytes("RGB", (image_width, image_height), array)
    frame = np.array(frame)
    return frame






