from myfunctions import *

dir1 = "/home/theodore/Documents/ECE/diplomatiki/NAO/snapshots10_2classes/green/"
dir2 = "/home/theodore/Documents/ECE/diplomatiki/NAO/snapshots11_2classes/green/"

# audio_path = "/home/nao/audioAthena/"
# audio_path = "/home/nao/audio/"

# get features for all images
X1, Y1, img_num1 = get_train_features(dir1)
X2, Y2, img_num2 = get_train_features(dir2)

# # train with only a few samples
# # sort according to label
# ind = np.argsort(Y1)
# Y1 = Y1[ind]
# X1 = X1[ind, :]
#
# # separate classes samples
# X1_0 = X1[0:81, :]
# Y1_0 = Y1[0:81]
# X1_100 = X1[82:104, :]
# Y1_100 = Y1[82:104]
#
# # get random rows from 2 groups of samples
# p = 0.4
# n_0 = np.int(X1_0.shape[0] * p)
# n_100 = np.int(X1_100.shape[0] * p)
# print n_0, n_100
#
# ind_0 = np.random.choice(X1_0.shape[0], n_0, replace=False)
# X1_0 = X1_0[ind_0, :]
# Y1_0 = Y1_0[ind_0]
#
# ind_100 = np.random.choice(X1_100.shape[0], n_100, replace=False)
# X1_100 = X1_100[ind_100, :]
# Y1_100 = Y1_100[ind_100]
#
# X1_b = np.concatenate((X1_0, X1_100), axis=0)
# Y1_b = np.concatenate((Y1_0, Y1_100), axis=0)


# select_features = [0, 1, 2, 3, 4]
# Xr = X[:, select_features]
# Xr = X

# best grasping
c1 = np.mean(X1[np.where(Y1 == 100), :], axis=1)
c2 = np.mean(X2[np.where(Y2 == 100), :], axis=1)

# train classifier 1
clf1 = svm.SVC(kernel='rbf', gamma=0.0001, C=10, probability=True)
print np.mean(model_selection.cross_val_score(clf1, X1, Y1, cv=10))
clf1.fit(X1, Y1)

# train classifier 1
clf2 = svm.SVC(kernel='rbf', gamma=0.0001, C=10, probability=True)
print np.mean(model_selection.cross_val_score(clf2, X2, Y2, cv=10))
clf2.fit(X2, Y2)

# connect to NAO
IP = "192.168.0.108"
PORT = 9559
ttsProxy = ALProxy("ALTextToSpeech", IP, PORT)
motionProxy = ALProxy("ALMotion", IP, PORT)
videoProxy = ALProxy("ALVideoDevice", IP, PORT)
photoCaptureProxy = ALProxy("ALPhotoCapture", IP, PORT)
postureProxy = ALProxy("ALRobotPosture", IP, PORT)
audioProxy = ALProxy("ALAudioPlayer", IP, PORT)

# initialize camera
subscriber = init_cam(videoProxy)

# train color gaussian model
mean, cov = train_color_model()

# initialize joints
speed = 0.2
nao_stiffness(IP, PORT, 1.0)
# nao_posture(IP, PORT, "Crouch")
init_joints(motionProxy, speed)
init_head(IP, PORT)

# store object's last 5 positions
history = Queue.Queue(maxsize=5)
grasped_it = False
cnt_grasp = 0
cnt_bad_dim = 0
X = X1
Y = Y1
clf = clf1
c = c1

# plot samples
# plt.ion()
# plt.scatter(X[:, 0], X[:, 1], s=50, c=Y)
# plt.draw()
# plt.show()

# time.sleep(2)
# ttsProxy.say("Please give me the object")
# audioProxy.playFile(audio_path + "DoseMou.wav")

y = np.zeros(5)

while True:
    frame = cam2numpy(videoProxy, subscriber)  # read image from NAO camera

    found, contour, thresh = get_object_contour(frame, "green")  # object contour

    if grasped_it and (not found):
        # ttsProxy.say("Oops! Give it to me again!")
        grasped_it = False
        motionProxy.setAngles(RHandName, 1.0, 0.2)

    if found:
        box = contour2box(contour)  # contour bounding box

        features = cx, cy, orientation, width, height = contour_features(contour)  # contour features
        features = np.array(features)

        history, is_steady, sx, sy = is_object_steady(history, cx, cy)  # child holds the object steady?

        if grasped_it:
            x = np.reshape(features, (1, -1))
            Y_pred = clf.predict(x)
            y[cnt_grasp] = Y_pred
            cnt_grasp += 1
            if cnt_grasp >= 5:
                if np.sum(y) < 5 * 100:  # not is_steady:
                    # ttsProxy.say("Oops! Give it to me again!")
                    grasped_it = False
                    cnt_grasp = 0
                    y = np.zeros(5)
                    motionProxy.setAngles(RHandName, 1.0, 0.2)
                else:
                    # ttsProxy.say("Thank you!")
                    exit()

        is_steady = True
        if is_steady:
            # x = [features[select_features]]
            x = features
            x = np.reshape(x, (1, -1))
            Y_pred = clf.predict(x)  # predict grasping quality

            bad_dim = np.argmax(np.abs(x - c))
            # print nao_response(bad_dim)
            if bad_dim == 0:
                cnt_bad_dim += 1
                if cnt_bad_dim >= 5:
                    err = np.abs(x[0][0] - c[0][0])
                    # phi = 0.03 * err
                    # nao_move_shoulder(IP, PORT, phi)
                    nao_move_shoulder(IP, PORT, 5)
                    # nao_move_head(IP, PORT, 5)
                    clf = clf2
                    c = c2
                    cnt_bad_dim = 0

            if Y_pred == 0:                 # no handover
                color = (0, 0, 255)
            elif Y_pred == 50:              # average handover
                color = (255, 255, 255)
            elif Y_pred == 100:             # good handover
                color = (255, 0, 0)
                motionProxy.setAngles(RHandName, 0.0, 0.2)
                grasped_it = True

        cv2.drawContours(frame, [box], 0, color, 2)  # draw detected object bounding box
        cv2.circle(frame, (cx, cy), 5, color, thickness=3)

        # show object in feature space
        # plt.ion()
        # plt.scatter(x[0][0], x[0][1], s=50, c="green")
        # plt.draw()

    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)  # show frame
    cv2.imshow("Frame", frame)
    # cv2.imshow("Frame", np.concatenate((frame, np.dstack([thresh] * 3)), axis=1))

    key = cv2.waitKey(1) & 0xFF  # press q to exit
    if key == ord("q"):
        break

videoProxy.unsubscribe(subscriber)


