from myfunctions import *


# connect to NAO
IP = "192.168.0.108"
PORT = 9559
ttsProxy = ALProxy("ALTextToSpeech", IP, PORT)
motionProxy = ALProxy("ALMotion", IP, PORT)
videoProxy = ALProxy("ALVideoDevice", IP, PORT)
photoCaptureProxy = ALProxy("ALPhotoCapture", IP, PORT)
postureProxy = ALProxy("ALRobotPosture", IP, PORT)
audioProxy = ALProxy("ALAudioPlayer", IP, PORT)

subscriber = init_cam(videoProxy)  # initialize camera

mean, cov = train_color_model()  # train color gaussian model

# initialize joints
speed = 0.2
nao_stiffness(IP, PORT, 1.0)
# nao_posture(IP, PORT, "Crouch")
init_joints(motionProxy, speed)

y = np.zeros(5)

while True:
    frame = cam2numpy(videoProxy, subscriber)  # read image from NAO camera
    marker_found, marker_contour, _ = get_object_contour(frame, "marker")  # marker contour

    x_ref = 580
    y_ref = 125
    cv2.circle(frame, (x_ref, y_ref), 6, (0, 255, 255), thickness=3)  # reference position

    if marker_found:
        box = contour2box(marker_contour)  # contour bounding box

        features = cx, cy, orientation, width, height = contour_features(marker_contour)  # contour features
        features = np.array(features)

        cv2.circle(frame, (cx, cy), 5, (255, 0, 255), thickness=2)

        delta_x = (x_ref - cx) / 640.0
        delta_y = (y_ref - cx) / 480
        print delta_x, delta_y

        if np.abs(delta_x) >= 0.2:
            K = 10
            delta_yaw = K * delta_x
            head_yaw = motionProxy.getAngles(headYawName, False)[0] * almath.TO_DEG
            head_yaw += delta_yaw
            motionProxy.setAngles(headYawName, head_yaw, 0.03)

    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)  # show frame
    cv2.imshow("Frame", frame)

    key = cv2.waitKey(1) & 0xFF  # press q to exit
    if key == ord("q"):
        break

videoProxy.unsubscribe(subscriber)


