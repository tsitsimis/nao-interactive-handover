from myfunctions import *

# samples directories
dir1 = "/home/theodore/Documents/ECE/diplomatiki/NAO/snapshots10_2classes/green/"
dir2 = "/home/theodore/Documents/ECE/diplomatiki/NAO/snapshots11_2classes/green/"

# get features for all images
X1, Y1, img_num1 = get_train_features(dir1)
X2, Y2, img_num2 = get_train_features(dir2)

# best grasping
c1 = np.mean(X1[np.where(Y1 == 100), :], axis=1)
c2 = np.mean(X2[np.where(Y2 == 100), :], axis=1)

# train classifier 1
clf1 = svm.SVC(kernel='rbf', gamma=0.0001, C=10, probability=True)
print np.mean(model_selection.cross_val_score(clf1, X1, Y1, cv=10))
clf1.fit(X1, Y1)

# train classifier 1
clf2 = svm.SVC(kernel='rbf', gamma=0.0001, C=10, probability=True)
print np.mean(model_selection.cross_val_score(clf2, X2, Y2, cv=10))
clf2.fit(X2, Y2)

# connect to NAO
IP = "192.168.0.108"
PORT = 9559
ttsProxy = ALProxy("ALTextToSpeech", IP, PORT)
motionProxy = ALProxy("ALMotion", IP, PORT)
videoProxy = ALProxy("ALVideoDevice", IP, PORT)
photoCaptureProxy = ALProxy("ALPhotoCapture", IP, PORT)
postureProxy = ALProxy("ALRobotPosture", IP, PORT)
audioProxy = ALProxy("ALAudioPlayer", IP, PORT)

subscriber = init_cam(videoProxy)  # initialize camera

mean, cov = train_color_model()  # train color gaussian model

# initialize joints
speed = 0.2
nao_stiffness(IP, PORT, 1.0)
# nao_posture(IP, PORT, "Crouch")
init_joints(motionProxy, speed)

# store object's last 5 positions
history = Queue.Queue(maxsize=5)
grasped_it = False
cnt_grasp = 0
cnt_bad_dim = 0
X = X1
Y = Y1
clf = clf1
c = c1

# init shoulder to head interpolation
tck = init_interpolation()

# ttsProxy.say("Please give me the object")

y = np.zeros(5)

while True:
    frame = cam2numpy(videoProxy, subscriber)  # read image from NAO camera

    object_found, contour, thresh = get_object_contour(frame, "green")  # object contour

    marker_found, marker_contour, _ = get_object_contour(frame, "marker")   # marker contour
    box = contour2box(marker_contour)                                       # contour bounding box
    m_x, m_y, _, _, _ = contour_features(marker_contour)                    # contour features
    cv2.circle(frame, (m_x, m_y), 5, (255, 0, 255), thickness=2)            # draw marker position

    if grasped_it and (not object_found):
        # ttsProxy.say("Oops! Give it to me again!")
        grasped_it = False
        motionProxy.setAngles(RHandName, 1.0, 0.2)

    if object_found:
        box = contour2box(contour)  # contour bounding box

        features = cx, cy, orientation, width, height = contour_features(contour)  # contour features
        features = np.array(features)

        head_follow_marker(IP, PORT, m_x, m_y, cx, cy)

        history, is_steady, sx, sy = is_object_steady(history, cx, cy)  # child holds the object still?

        if grasped_it:
            x = np.reshape(features, (1, -1))
            Y_pred = clf.predict(x)
            y[cnt_grasp] = Y_pred
            cnt_grasp += 1
            if cnt_grasp >= 5:
                if np.sum(y) < 5 * 100:  # not is_steady:
                    # ttsProxy.say("Oops! Give it to me again!")
                    grasped_it = False
                    cnt_grasp = 0
                    y = np.zeros(5)
                    motionProxy.setAngles(RHandName, 1.0, 0.2)
                else:
                    # ttsProxy.say("Thank you!")
                    exit()

        is_steady = True
        if is_steady:
            x = features
            x = np.reshape(x, (1, -1))
            Y_pred = clf.predict(x)  # predict grasping quality

            bad_dim = np.argmax(np.abs(x - c))
            if bad_dim == 0 and False:
                cnt_bad_dim += 1
                if cnt_bad_dim >= 5:
                    err = np.abs(x[0][0] - c[0][0])
                    phi = 2 * almath.TO_RAD
                    move, shoulder_pitch = nao_move_shoulder(IP, PORT, phi)
                    cnt_bad_dim = 0

            if Y_pred == 0:                 # no handover
                color = (0, 0, 255)
            elif Y_pred == 50:              # average handover
                color = (255, 255, 255)
            elif Y_pred == 100:             # good handover
                color = (255, 0, 0)
                motionProxy.setAngles(RHandName, 0.0, 0.2)
                grasped_it = True

        cv2.drawContours(frame, [box], 0, color, 2)  # draw detected object bounding box
        cv2.circle(frame, (cx, cy), 5, color, thickness=3)

        # show object in feature space
        # plt.ion()
        # plt.scatter(x[0][0], x[0][1], s=50, c="green")
        # plt.draw()

    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)  # show frame
    cv2.imshow("Frame", frame)
    # cv2.imshow("Frame", np.concatenate((frame, np.dstack([thresh] * 3)), axis=1))

    key = cv2.waitKey(1) & 0xFF  # press q to exit
    if key == ord("q"):
        break

videoProxy.unsubscribe(subscriber)


