from myfunctions import *

object_color = "blue_rectangle"
dir = "/home/theodore/Documents/ECE/diplomatiki/NAO/snapshots13/" + object_color + "/"

# audio_path = "/home/nao/audioAthena/"
# audio_path = "/home/nao/audio/"

# get features for all images
X, Y, img_num = get_train_features(dir, object_color)

# best grasping
c = np.mean(X[np.where(Y == 100), :], axis=1)

# train classifier
gamma_o = 1e-5
C_o = 10
clf = svm.SVC(kernel="rbf", gamma=gamma_o, C=C_o, probability=True)
print np.mean(model_selection.cross_val_score(clf, X, Y, cv=10))
# exit()
clf.fit(X, Y)

# clf = neighbors.KNeighborsClassifier(n_neighbors=5)
# clf.fit(X, Y)

# connect to NAO
IP = "192.168.0.109"
PORT = 9559
ttsProxy = ALProxy("ALTextToSpeech", IP, PORT)
motionProxy = ALProxy("ALMotion", IP, PORT)
videoProxy = ALProxy("ALVideoDevice", IP, PORT)
photoCaptureProxy = ALProxy("ALPhotoCapture", IP, PORT)
postureProxy = ALProxy("ALRobotPosture", IP, PORT)
audioProxy = ALProxy("ALAudioPlayer", IP, PORT)

# initialize camera
subscriber = init_cam(videoProxy)

# train color gaussian model
# mean, cov = train_color_model()

# initialize joints
speed = 0.2
nao_stiffness(IP, PORT, 1.0)
# nao_posture(IP, PORT, "StandInit")
# init_joints(motionProxy, speed)
init_arm(IP, PORT)
init_head(IP, PORT)

# store object's last 5 positions
history = Queue.Queue(maxsize=5)
grasped_it = False
cnt_grasp = 0

# plot samples
# plt.ion()
# plt.scatter(X[:, 0], X[:, 1], s=50, c=Y)
# plt.draw()
# plt.show()

# time.sleep(2)
# ttsProxy.say("Please give me the object")
# audioProxy.playFile(audio_path + "DoseMou.wav")
score_vals = np.zeros(1)
y = np.zeros(5)

while True:
    frame = cam2numpy(videoProxy, subscriber)  # read image from NAO camera

    found, contour, thresh, contours = get_object_contour2(frame, object_color)  # object contour

    if grasped_it and (not found):
        # ttsProxy.say("Oops! Give it to me again!")
        grasped_it = False
        motionProxy.setAngles(RHandName, 1.0, 0.2)

    if found:
        box = contour2box(contour)  # contour bounding box

        features = cx, cy, orientation, width, height = contour_features(contour)  # contour features
        features = np.array(features)

        # score = detection_score(contour, width, height, contours)
        score = 1.0
        # print score
        # score_vals = np.concatenate((score_vals, np.array([score[2]])))

        # history, is_steady, sx, sy = is_object_steady(history, cx, cy)  # child holds the object still?

        if grasped_it:
            x = np.reshape(features, (1, -1))
            Y_pred = clf.predict(x)
            y[cnt_grasp] = Y_pred
            cnt_grasp += 1
            if cnt_grasp >= 5:
                if np.sum(y) < 5 * 100:  # not is_steady:
                    # ttsProxy.say("Oops! Give it to me again!")
                    grasped_it = False
                    cnt_grasp = 0
                    y = np.zeros(5)
                    motionProxy.setAngles(RHandName, 1.0, 0.2)
                else:
                    # ttsProxy.say("Thank you!")
                    break

        is_steady = True
        if is_steady and score >= 0.8:
            # x = [features[select_features]]
            x = features
            x = np.reshape(x, (1, -1))
            Y_pred = clf.predict(x)  # predict grasping quality

            print Y_pred
            if Y_pred == 0:                 # no handover
                color = (0, 0, 255)
                text = "bad"
            elif Y_pred == 50:              # average handover
                color = (255, 255, 255)
                text = "average"
            elif Y_pred == 100:             # good handover
                color = (255, 0, 0)
                text = "good"
                motionProxy.setAngles(RHandName, 0.0, 0.2)
                grasped_it = True

        cv2.drawContours(frame, [box], 0, color, 2)  # draw detected object bounding box
        cv2.circle(frame, (cx, cy), 5, color, thickness=3)
        cv2.putText(frame, text, (np.int(cx), np.int(cy)), cv2.FONT_HERSHEY_COMPLEX_SMALL,
                                  1, color, 2)

        # show object in feature space
        # plt.ion()
        # plt.scatter(x[0][0], x[0][1], s=50, c="green")
        # plt.draw()

    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)  # show frame
    # cv2.imshow("Frame", frame)
    cv2.imshow("Frame", np.concatenate((frame, np.dstack([thresh] * 3)), axis=1))

    key = cv2.waitKey(1) & 0xFF  # press q to exit
    if key == ord("q"):
        break

videoProxy.unsubscribe(subscriber)

# plt.plot(score_vals)
# plt.show()
# plt.hist(score_vals, bins='auto')
# plt.title("Histogram of detection scores")
# plt.show()
# print score_vals


