import numpy as np
from scipy import interpolate
import matplotlib.pyplot as plt


# While I was measuring the position of the marker every time
# NAO lifted his arm, I noticed deviations on the marker position

marker_pos = np.array([[580, 125], [578, 133], [580, 125], [564, 135], [573, 128], [574, 147]])

marker_pos_std = [np.std(marker_pos[:, 0]), np.std(marker_pos[:, 1])]


data = np.array([[9.8, 0], [12, 2], [14, 3], [16, 6], [18, 7]])

tck = interpolate.splrep(data[:, 0], data[:, 1], s=0)
xnew = np.arange(9, 19, 0.5)
ynew = interpolate.splev(xnew, tck, der=0)


# plot
plt.plot(data[:, 0], data[:, 1], "x",
         xnew, ynew, "b",
         15, interpolate.splev(15, tck, der=0), "x")
plt.xlabel("sholder roll")
plt.ylabel("head yaw")
plt.show()


