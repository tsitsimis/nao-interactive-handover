import numpy as np
import cv2
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import matplotlib.ticker as ticker
import os
from naoqi import ALProxy
import vision_definitions as vd
import Image
from sklearn import metrics, model_selection
from sklearn import svm, linear_model, neighbors, tree, neural_network, naive_bayes
from scipy.stats import multivariate_normal
import almath
import time
import Queue


def get_object_contour(img):
    # convert to HSV color space
    hsv = cv2.cvtColor(img, cv2.COLOR_RGB2HSV)
    # threshold
    green_lower = (42, 42, 56)
    green_upper = (83, 255, 255)
    thresh = cv2.inRange(hsv, green_lower, green_upper)

    # opening
    kernel = np.ones((3, 3), np.uint8)
    opening = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel, iterations=2)
    closing = cv2.morphologyEx(opening, cv2.MORPH_OPEN, kernel, iterations=2)

    opening = cv2.morphologyEx(closing, cv2.MORPH_OPEN, kernel, iterations=5)

    contours = cv2.findContours(opening.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[-2]
    if len(contours) > 0:
        c = max(contours, key=cv2.contourArea)
        return True, c
    return False, []


def bin_image(proba_img, t=1):
    thresh = cv2.inRange(proba_img, t, 255)
    return thresh


def get_object_contour2(bin_img):
    thresh = bin_img

    # opening
    kernel = np.ones((3, 3), np.uint8)
    opening = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel, iterations=2)
    closing = cv2.morphologyEx(opening, cv2.MORPH_OPEN, kernel, iterations=2)

    opening = cv2.morphologyEx(closing, cv2.MORPH_OPEN, kernel, iterations=5)

    contours = cv2.findContours(opening.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[-2]
    if len(contours) > 0:
        c = max(contours, key=cv2.contourArea)
        return True, c
    return False, []


def contour2box(c):
    rect = cv2.minAreaRect(c)
    box = cv2.boxPoints(rect)
    box = box.astype(int)
    return box


def contour_features(contour):
    # fit rectangle
    ((_, _), (width, height), angle) = cv2.minAreaRect(contour)  # rectangle

    # centroid
    mom = cv2.moments(contour)
    cx = int(mom['m10'] / mom['m00'])
    cy = int(mom['m01'] / mom['m00'])

    # orientation
    if width > height:
        orientation = np.abs(angle)
    else:
        orientation = (90 - np.abs(angle))

    # rectangle corners
    nw_x = cx - width / 2
    nw_y = cy - height / 2

    ne_x = cx + width / 2
    ne_y = cy - height / 2

    sw_x = cx - width / 2
    sw_y = cy + height / 2

    se_x = cx + width / 2
    se_y = cy + height / 2

    # features vector
    # features = [cx, cy, orientation, width, height]
    features = [cx, cy, nw_x, nw_y, ne_x, ne_y, sw_x, sw_y, se_x, se_y, orientation, width, height]
    return features


def get_train_features(directory):
    files = os.listdir(directory)
    n_samples = len(files)
    n_features = 13

    X = np.zeros((n_samples, n_features))
    Y = np.zeros(n_samples, dtype=int)
    img_num = np.zeros(n_samples)

    for i in range(0, n_samples):
        img_name = files[i]
        Y[i], img_num[i] = parse_filename(img_name)

        full_path = directory + img_name
        img = cv2.imread(full_path)
        found, contour = get_object_contour(img)
        if found:
            X[i, :] = contour_features(contour)

    return X, Y, img_num


def parse_filename(img_name):
    components = img_name.split("_")

    # get img number
    num = components[1]
    num = int(num)

    # get img score
    score = components[2]
    score = score.split('.')[0]
    # score = float(score) / 100
    score = np.int(score)

    return score, num


def create_train_test(features, p):
    n_samples = np.shape(features)[0]
    all_ind = range(0, n_samples)

    # train set
    n_train = np.ceil(p * n_samples)
    train_ind = np.random.choice(all_ind, n_train, replace=False)
    train_set = features[train_ind, :]

    # test set
    test_ind = [x for x in all_ind if x not in train_ind]
    test_set = features[test_ind, :]

    return train_ind, train_set, test_ind, test_set


def plot_decision_boundary(clf, X, Y):
    X = X[:, [0, 1]]
    clf.fit(X, Y)
    h = 0.5
    x_min, x_max = X[:, 0].min() - 1, X[:, 0].max() + 1
    y_min, y_max = X[:, 1].min() - 1, X[:, 1].max() + 1
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))
    Z = clf.predict(np.c_[xx.ravel(), yy.ravel()])
    Z = Z.reshape(xx.shape)
    plt.contourf(xx, yy, Z, cmap=plt.cm.coolwarm, alpha=0.8)
    plt.scatter(X[:, 0], X[:, 1], c=Y, cmap=plt.cm.coolwarm)
    plt.show()

    # plt.xlabel('centroid x')
    # plt.ylabel('orientation')
    # plt.title('decision boundaries')
    #
    # classes = ['good', 'medium', 'bad']
    # class_colours = ['red', 'white', 'blue']
    # recs = []
    # for i in range(0,len(class_colours)):
    #     recs.append(mpatches.Rectangle((0, 0), 1, 1, fc=class_colours[i]))
    # plt.legend(recs,classes,loc=4)


def is_object_steady(history, cx, cy):
    # add new position
    history.put([cx, cy])

    # convert to numpy array
    history_np = np.array(list(history.queue))

    # calc std of x, y coordinates
    sx = np.std(history_np[:, 0])
    sy = np.std(history_np[:, 1])

    # remove oldest position
    if history.full():
        history.get()

    # check if object is steady
    is_steady = (sx <= 30 and sy <= 30)

    return history, is_steady


def nao_response(f):
    if f == 0:
        return "Bring the object closer to the x axis"
    elif f == 1:
        return "Bring the object closer to the y axis"
    elif f == 2:
        return "Rotate the object"
    elif f == 3:
        return "Bring the object closer to the z axis"


def train_color_model():
    my_dir = "/home/theodore/Documents/ECE/diplomatiki/NAO/code/python/nao_handover/color_samples/txt/"
    files = os.listdir(my_dir)
    n_samples = len(files)

    samples = np.zeros((0, 3))

    for i in range(0, n_samples):
        full_path = my_dir + files[i]
        color_points = np.loadtxt(full_path)
        samples = np.concatenate((samples, color_points), axis=0)

    mean = np.mean(samples, axis=0)
    cov = np.cov(samples, rowvar=0)

    return mean, cov


def proba_image(img, mean, cov, d3=True):
    proba_img = multivariate_normal.pdf(img, mean, cov)
    proba_img /= np.max(proba_img)
    proba_img *= 255

    if d3:
        proba_img = np.dstack([proba_img] * 3)

    return proba_img


def accuracy_heatmap(X, Y, c, gamma):
    clen = len(c)
    glen = len(gamma)
    amap = np.zeros((clen, glen))

    for i in range(clen):
        c1 = c[i]
        for j in range(glen):
            gamma1 = gamma[j]
            clf_svm = svm.SVC(kernel='rbf', gamma=gamma1, C=c1, probability=True)
            cross_val_svm = model_selection.cross_val_score(clf_svm, X, Y, cv=10)
            amap[i, j] = np.mean(cross_val_svm)

    plt.imshow(amap, cmap='hot', interpolation='nearest')
    plt.xlabel('gamma')
    plt.ylabel('C')
    plt.colorbar()
    plt.xticks(np.arange(len(gamma)), gamma, rotation=45)
    plt.yticks(np.arange(len(c)), c)
    plt.title('Validation accuracy')
    plt.show()


def n_samples_classify(X, Y, n_samples):
    acc = np.zeros(len(n_samples))
    for i in range(len(n_samples)):
        k = n_samples[i]
        ind = np.random.choice(X.shape[0], k, replace=False)
        X_ = X[ind, :]
        Y_ = Y[ind]
        clf_svm = svm.SVC(kernel='rbf', gamma=0.0001, C=10, probability=True)
        cross_val_svm = model_selection.cross_val_score(clf_svm, X_, Y_, cv=10)

        acc[i] = np.mean(cross_val_svm)

    y_pos = np.arange(len(n_samples))
    plt.bar(y_pos, acc, align='center', alpha=0.5)
    plt.xticks(y_pos, n_samples)
    plt.xlabel('number of train samples')
    plt.ylabel('Accuracy')
    plt.show()


def show_detection_parts(img, my_dir):
    img = cv2.imread(my_dir + "green_33_50.jpg")
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    # convert to HSV color space
    hsv = cv2.cvtColor(img, cv2.COLOR_RGB2HSV)
    # threshold
    green_lower = (42, 42, 56)
    green_upper = (83, 255, 255)
    thresh = cv2.inRange(hsv, green_lower, green_upper)
    # opening
    kernel = np.ones((3, 3), np.uint8)
    opening = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel, iterations=2)
    closing = cv2.morphologyEx(opening, cv2.MORPH_CLOSE, kernel, iterations=2)
    # contour
    contours = cv2.findContours(opening.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[-2]
    c = max(contours, key=cv2.contourArea)
    # ((_, _), (width, height), angle) = cv2.minAreaRect(c)
    rect = cv2.minAreaRect(c)
    box = cv2.boxPoints(rect)
    box = box.astype(int)

    final_img = img.copy()
    cv2.drawContours(final_img, [box], 0, (255, 0, 0), 6)

    # plt.imshow(img)
    # plt.imshow(hsv)
    # plt.imshow(thresh, cmap='gray')
    # plt.imshow(closing, cmap='gray')
    plt.imshow(final_img)
    plt.show()
