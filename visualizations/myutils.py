import numpy as np
import matplotlib.pyplot as plt


def vis_2classes_2d_boundary():
    mu1 = [-1, 1]
    cov1 = 0.2 * np.eye(2)

    mu2 = [1, -1]
    cov2 = 0.1 * np.eye(2)

    # data
    x1 = np.random.multivariate_normal(mu1, cov1, 100)
    x2 = np.random.multivariate_normal(mu2, cov2, 50)

    # boundary
    t = np.linspace(-2, 2, 100)
    yb = t

    plt.scatter(x1[:, 0], x1[:, 1], c='yellow')
    plt.scatter(x2[:, 0], x2[:, 1], c='red')
    plt.plot(t, yb, c='black', lw=2)
    plt.axis('equal')
    plt.xlabel(r'$f_1$')
    plt.ylabel(r'$f_2$')
    plt.show()


def vis_2classes_1d_boundary():
    mu1 = 300
    sigma1 = 0.2

    mu2 = 301
    sigma2 = 0.3

    # data
    n1 = 10
    n2 = 10
    x1 = np.random.normal(mu1, sigma1, n1)
    x2 = np.random.normal(mu2, sigma2, n2)

    good = plt.scatter(x1, np.zeros(n1), c='yellow', s=100)
    bad = plt.scatter(x2, np.zeros(n2), c='cyan', s=100)
    plt.vlines(300.5, -0.5, 0.5, linestyles='dashed', linewidth=2)
    plt.axis('equal')
    plt.xlabel(r'$x_c$')
    plt.legend((good, bad), ('good grasping', 'bad grasping'), scatterpoints=1, loc='lower left')
    plt.show()


