from myfunctions import *


# train color model
# mean, cov = train_color_model()
dir = "/home/theodore/Documents/ECE/diplomatiki/NAO/code/python/gaussian_color_classification/samples/lab_blue/"
# mean, cov = fit_model(color_points, 2)

mean, cov = train_color_model(dir)
mean = mean[0]
cov = cov[0]


# test
# connect to NAO
IP = "169.254.28.162"
PORT = 9559

# ttsProxy = ALProxy("ALTextToSpeech", IP, PORT)
# motionProxy = ALProxy("ALMotion", IP, PORT)
# videoProxy = ALProxy("ALVideoDevice", IP, PORT)
# photoCaptureProxy = ALProxy("ALPhotoCapture", IP, PORT)
# postureProxy = ALProxy("ALRobotPosture", IP, PORT)

# subscriber = init_cam(videoProxy)
camera = cv2.VideoCapture(0)
while True:
    # read image from NAO camera
    # frame = cam2numpy(videoProxy, subscriber)
    (grabbed, frame) = camera.read()
    frame = np.flip(frame, axis=1)
    # frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

    # method 1
    proba_img = proba_image(frame, mean, cov)
    mask1 = get_contour(proba_img, 10)

    # method 2
    mask2 = get_contour2(frame)

    # show results
    images_to_show = [frame, proba_img, mask1, mask2]
    cv2.namedWindow("montage", cv2.WINDOW_NORMAL)
    montages = build_montages(images_to_show,  (640/2, 480/2), (len(images_to_show), 1))
    for montage in montages:
        cv2.imshow("montage", montage)
        # cv2.waitKey()

    # press q to exit
    key = cv2.waitKey(1) & 0xFF
    if key == ord("q"):
        break


# videoProxy.unsubscribe(subscriber)
