from myfunctions import *

my_dir = "/home/theodore/Documents/ECE/diplomatiki/NAO/snapshots8/green/"

# show bounding box of a single sample image
# show_box(my_dir, "green_30_50.jpg")
# quit()

# get features for all images
X, Y, img_num = get_train_features(my_dir)

select_features = [0, 1, 2, 3, 4]
Xr = X[:, select_features]

# print np.where(Y != 50)[0]
# exit()

# train classifier
clf = svm.SVC(kernel='rbf', gamma=0.001, C=1)
# clf = neighbors.KNeighborsClassifier(n_neighbors=5)
# clf.fit(X, Y)


# tab = np.concatenate((np.array([num]).T, np.array([Y]).T, Xr), axis=1)
# tab100 = tab[np.where(tab[:, 1] == 100), :][0]
# tabbig = tab100[np.where(tab100[:, 3] > 200), :][0]
# print tabbig
# quit()

# cross validation
scores = model_selection.cross_val_score(clf, Xr, Y, cv=10)
mean_score = np.mean(scores)
print mean_score

# plot features
X_2d = X[:, [0, 1]]
plot_decision_boundary(clf, X_2d, Y)
plt.show()
