import numpy as np
import cv2
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import os
from sklearn import svm, metrics, model_selection, neighbors


def get_object_contour(img):
    # convert to HSV color space
    hsv = cv2.cvtColor(img, cv2.COLOR_RGB2HSV)

    # threshold
    blueLower = (88, 66, 0)
    blueUpper = (255, 255, 255)
    green_lower = (42, 42, 56)
    green_upper = (83, 255, 255)
    thresh = cv2.inRange(hsv, green_lower, green_upper)

    # opening
    kernel = np.ones((3, 3), np.uint8)
    opening = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel, iterations=2)
    closing = cv2.morphologyEx(opening, cv2.MORPH_OPEN, kernel, iterations=2)

    opening = cv2.morphologyEx(closing, cv2.MORPH_OPEN, kernel, iterations=5)

    contours = cv2.findContours(opening.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[-2]
    if len(contours) > 0:
        c = max(contours, key=cv2.contourArea)
        return True, c
    return False, []


def contour_features(contour):
    # fit rectangle
    rect = ((x, y), (width, height), angle) = cv2.minAreaRect(contour)

    # centroid
    M = cv2.moments(contour)
    cx = int(M['m10'] / M['m00'])
    cy = int(M['m01'] / M['m00'])

    # orientation
    if width > height:
        orientation = np.abs(angle)
    else:
        orientation = (90 - np.abs(angle))

    # features vector
    features = [cx, cy, orientation, width, height]
    return features


def get_train_features(directory):
    files = os.listdir(directory)
    n_samples = len(files)
    n_features = 5

    X = np.zeros((n_samples, n_features))
    Y = np.zeros(n_samples, dtype=int)
    img_num = np.zeros(n_samples)

    for i in range(0, n_samples):
        img_name = files[i]
        Y[i], img_num[i] = parse_filename(img_name)

        full_path = directory + img_name
        img = cv2.imread(full_path)
        found, contour = get_object_contour(img)
        if found:
            X[i, :] = contour_features(contour)

    return X, Y, img_num


def parse_filename(img_name):
    components = img_name.split("_")

    # get img number
    num = components[1]
    num = int(num)

    # get img score
    score = components[2]
    score = score.split('.')[0]
    # score = float(score) / 100
    score = np.int(score)

    return score, num


def contour2box(c):
    rect = cv2.minAreaRect(c)
    box = cv2.boxPoints(rect)
    box = box.astype(int)
    return box


def get_score(img_name):
    components = img_name.split("_")

    # get img number
    num = components[1]
    num = int(num)

    # get img score
    score = components[2]
    score = score.split('.')[0]
    # score = float(score) / 100
    score = np.int(score)

    return score, num


def create_train_test(features, p):
    n_samples = np.shape(features)[0]
    all_ind = range(0, n_samples)

    # train set
    n_train = np.ceil(p * n_samples)
    train_ind = np.random.choice(all_ind, n_train, replace=False)
    train_set = features[train_ind, :]

    # test set
    test_ind = [x for x in all_ind if x not in train_ind]
    test_set = features[test_ind, :]

    return train_ind, train_set, test_ind, test_set


def plot_decision_boundary(clf, X, Y):
    h = 0.5
    x_min, x_max = X[:, 0].min() - 1, X[:, 0].max() + 1
    y_min, y_max = X[:, 1].min() - 1, X[:, 1].max() + 1
    xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))
    clf.fit(X, Y)
    Z = clf.predict(np.c_[xx.ravel(), yy.ravel()])
    Z = Z.reshape(xx.shape)

    # plt.contourf(xx, yy, Z, cmap=plt.cm.coolwarm, alpha=0.8)
    plt.scatter(X[:, 0], X[:, 1], c=Y, cmap=plt.cm.coolwarm, edgecolors='k')
    # plt.scatter(X[:, 0], np.ones(X[:, 0].shape[0]), c=Y, cmap=plt.cm.coolwarm, edgecolors='k')
    # plt.title('decision boundaries')
    # plt.title('c_x feature')
    plt.xlabel(r'$c_x$')
    plt.ylabel(r'$c_y$')

    # classes = ['good', 'medium', 'bad']
    classes = ['good', 'bad']
    class_colours = ['red', 'white', 'blue']
    recs = []
    for i in [0, 2]:
        recs.append(mpatches.Rectangle((0, 0), 1, 1, fc=class_colours[i]))
    plt.legend(recs, classes, loc=4)

    plt.show()


def show_box(my_dir, img_name):
    img_path = my_dir + img_name
    img = cv2.imread(img_path)
    contour = get_object_contour(img)
    box = contour2box(contour)
    cv2.drawContours(img, [box], 0, (0, 0, 255), 2)
    plt.imshow(img)
    plt.show()

