from myfunctions import *

object_color = "green"
train_dir = "/home/theodore/Documents/ECE/diplomatiki/NAO/snapshots13/" + object_color + "/"
test_dir = "/home/theodore/Documents/ECE/diplomatiki/NAO/snapshots14/y-10p30/"

# get features for all samples
X, Y, img_num = get_train_features(train_dir, object_color)

# train with all samples
gamma_o = 1e-5
C_o = 10
clf = svm.SVC(kernel="rbf", gamma=gamma_o, C=C_o, probability=True)
print "Accuracy: %f" % np.mean(model_selection.cross_val_score(clf, X, Y, cv=10))
clf.fit(X, Y)

# test
X_test, Y_test, img_num_test = get_train_features(test_dir, object_color)
Y_pred = clf.predict(X_test)
print "Test accuracy: %f" % metrics.accuracy_score(Y_test, Y_pred)
c = metrics.confusion_matrix(Y_test, Y_pred, labels=[0, 50, 100])
print c

plt.figure()
plot_confusion_matrix(c, classes=["bad", "average", "good"],
                      title='Confusion matrix', normalize=False)
plt.show()



