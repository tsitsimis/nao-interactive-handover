from myfunctions import *

object_color = "green"
samples_dir = "/home/theodore/Documents/ECE/diplomatiki/NAO/snapshots12/" + object_color + "/"

# get samples
X, Y, img_num = get_train_features(samples_dir, object_color)
X = preprocessing.scale(X)  # scale
Y[np.where(Y == 50)] = 0    # average is bad
n = X.shape[0]              # data size

# "good handover" samples
X_good = X[np.where(Y == 100)]
Y_good = 100 * np.ones(X_good.shape[0])

# "bad handover" samples
X_bad = X[np.where(Y == 0)]
Y_bad = 0 * np.ones(X_bad.shape[0])

n_experiments = 50
accuracy = np.zeros(n_experiments)
f1 = np.zeros(n_experiments)
conf = np.zeros((2, 2))
for ie in range(n_experiments):
    # create train/test
    X_train, X_test, Y_train, Y_test = model_selection.train_test_split(X_good, Y_good, test_size=0.3)

    # train gmm
    gmm = mixture.GaussianMixture(1, 'full').fit(X_train)
    mu = gmm.means_[0]
    cov = gmm.covariances_[0]
    cov_inv = np.linalg.inv(cov)

    theta = 12  # TODO: theta from formula
    # ft = one_class_thresh(X_train, theta, mu, cov_inv)
    # print "fT+ = %f" % ft

    # exit()
    # test
    X_test = np.concatenate((X_test, X_bad), axis=0)
    Y_test = np.concatenate((Y_test, Y_bad), axis=0)
    n_test = X_test.shape[0]

    Y_pred = np.zeros(n_test)
    for i in range(n_test):
        x = X_test[i, :].reshape(1, -1)
        mah = np.dot(np.dot(x - mu, cov_inv), (x - mu).T)
        if mah <= theta:
            Y_pred[i] = 100
        else:
            Y_pred[i] = 0

    accuracy[ie] = metrics.accuracy_score(Y_test, Y_pred)
    f1[ie] = metrics.f1_score(Y_test, Y_pred, average='macro')
    c = metrics.confusion_matrix(Y_test, Y_pred, labels=[0, 100])
    conf += c
    # print "accuracy: %f" % metrics.accuracy_score(Y_test, Y_pred)
    # print "F1: %f" % metrics.f1_score(Y_test, Y_pred, average='macro')

print np.mean(accuracy)
print np.mean(f1)

conf /= n_experiments
conf = conf.astype('float') / conf.sum(axis=1)[:, np.newaxis]
conf *= 100
print conf

exit()
# ##########
# ####### good
n_good = X_good.shape[0]
mah = np.zeros(n_good)
for i in range(n_good):
    x = X_good[i, :].reshape(1, -1)
    mah[i] = np.dot(np.dot(x - mu, cov_inv), (x - mu).T)

# print mah.min(), mah.max()

# ####### bad
n_bad = X_bad.shape[0]
mah = np.zeros(n_bad)
for i in range(n_bad):
    x = X_bad[i, :].reshape(1, -1)
    mah[i] = np.dot(np.dot(x - mu, cov_inv), (x - mu).T)

# print mah.min(), mah.max()