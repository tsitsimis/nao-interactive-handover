from myfunctions import *

object_color = "green"
samples_dir = "/home/theodore/Documents/ECE/diplomatiki/NAO/snapshots12/" + object_color + "/"

img = cv2.imread("/home/theodore/Documents/ECE/diplomatiki/NAO/snapshots12/green/green_1_100.jpg")
ff, hull, thresh, contours = get_convex_hull_contour(img, object_color)

cv2.drawContours(img, np.array(hull), 0, (255, 0, 0), 3)
cv2.imshow("img", img)
cv2.waitKey(0)

exit()
# get samples
X, Y, img_num = get_train_features(samples_dir, object_color)
X = preprocessing.scale(X)  # scale
Y[np.where(Y == 50)] = 0    # average is bad
n = X.shape[0]              # data size

# "good handover" samples
X_good = X[np.where(Y == 100)]
Y_good = 100 * np.ones(X_good.shape[0])

# "bad handover" samples
X_bad = X[np.where(Y == 0)]
Y_bad = 0 * np.ones(X_bad.shape[0])

X_min = np.min(X, axis=0)
X_max = np.max(X, axis=0)

X_grid = np.d


