from myfunctions import *

object_color = "green"
samples_dir = "/home/theodore/Documents/ECE/diplomatiki/NAO/snapshots13/" + object_color + "/"

# get features for all samples
X, Y, img_num = get_train_features(samples_dir, object_color)

# train with all samples
gamma_o = 1e-3
C_o = 1
clf = svm.SVC(kernel="rbf", gamma=gamma_o, C=C_o, probability=True)
# print "Accuracy: %f" % np.mean(model_selection.cross_val_score(clf, X, Y, cv=10))
# print "F1: %f" % np.mean(model_selection.cross_val_score(clf, X, Y, cv=10, scoring='f1_macro'))

# X_train, X_test, Y_train, Y_test = model_selection.train_test_split(X, Y, test_size=0.3)
# clf.fit(X_train, Y_train)
# Y_pred = clf.predict(X_test)
# c = metrics.confusion_matrix(Y_test, Y_pred, labels=[0, 50, 100])
# print 100 * c.astype('float') / c.sum(axis=1)[:, np.newaxis]
# print c
# exit()

# separate samples
X0 = X[np.where(Y == 0), :][0]
X50 = X[np.where(Y == 50), :][0]
X100 = X[np.where(Y == 100), :][0]

n0 = X0.shape[0]
n50 = X50.shape[0]
n100 = X100.shape[0]

# select random samples from every class
n_bad = [16]  # np.arange(1, 21)
n_good = [20]
n_experiments = 50
results = np.zeros((len(n_good), len(n_bad)))
conf = np.zeros((3, 3))

for i_good in range(len(n_good)):
    for i_bad in range(len(n_bad)):
        size0 = n_bad[i_bad]
        size50 = n_bad[i_bad]
        size100 = n_good[i_good]

        accuracy_arr = np.zeros(n_experiments)
        for i in range(n_experiments):

            i0 = np.random.randint(X0.shape[0], size=size0)
            i50 = np.random.randint(X50.shape[0], size=size50)
            i100 = np.random.randint(X100.shape[0], size=size100)

            s0 = X0[i0, :]
            s50 = X50[i50, :]
            s100 = X100[i100, :]

            # train
            X_new = np.concatenate((s0, s50, s100), axis=0)
            Y_new = np.concatenate((
                0 * np.ones(size0),
                50 * np.ones(size50),
                100 * np.ones(size100)
            ))

            clf.fit(X_new, Y_new)

            # test
            X0_test = X0
            X0_test = np.delete(X0_test, i0, axis=0)

            X50_test = X50
            X50_test = np.delete(X50_test, i50, axis=0)

            X100_test = X100
            X100_test = np.delete(X100_test, i100, axis=0)

            X_test = np.concatenate((X0_test, X50_test, X100_test), axis=0)

            Y_test = np.concatenate((
                0 * np.ones(X0_test.shape[0]),
                50 * np.ones(X50_test.shape[0]),
                100 * np.ones(X100_test.shape[0])
            ))

            Y_pred = clf.predict(X_test)
            accuracy_arr[i] = metrics.accuracy_score(Y_test, Y_pred)

            # confusion matrix
            c = metrics.confusion_matrix(Y_test, Y_pred, labels=[0, 50, 100])
            # c = 1.0 * c / c.sum(axis=1)[:, np.newaxis]
            # c *= 100
            # c = np.around(c, 0)
            conf += c

        results[i_good, i_bad] = np.mean(accuracy_arr)
        # print "Train size: %d" % (size0 + size50 + size100)
        # print "Mean accuracy (%d experiments): %f" % (n_experiments, np.mean(accuracy_arr))

print np.argmax(results, axis=1)
print np.max(results, axis=1)
# print results

conf /= n_experiments
plt.figure()
plot_confusion_matrix(conf, classes=["bad", "average", "good"],
                      title='Confusion matrix', normalize=True)
plt.show()

exit()

# plot
fig, ax = plt.subplots()
marker = "x"
l1, = ax.plot(results[0, :], marker=marker, label="$n_{good} = 1$")
l2, = ax.plot(results[1, :], marker=marker, label="$n_{good} = 5$")
l3, = ax.plot(results[2, :], marker=marker, label="$n_{good} = 10$")
l4, = ax.plot(results[3, :], marker=marker, label="$n_{good} = 20$")
# l5, = ax.plot(results[4, :], marker=marker, label="$n_{good} = 25$")

plt.legend(handles=[l1, l2, l3, l4])
plt.xticks(range(len(n_bad)))
ax.set_xticklabels(labels=list(n_bad.astype(str)))
plt.xlabel("$n_{avg}, n_{bad}$")
plt.ylabel("accuracy")
plt.show()




