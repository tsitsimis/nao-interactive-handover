from myfunctions import *

samples_dir = "/home/theodore/Documents/ECE/diplomatiki/NAO/snapshots12/green/"

# get features for all samples
X, Y, img_num = get_train_features(samples_dir)

select_features = [0, 1, 2, 3, 4]
X = X[:, select_features]

# train
gamma_o = 1e-3
C_o = 10
clf = svm.SVC(kernel="rbf", gamma=gamma_o, C=C_o, probability=True)

print np.mean(model_selection.cross_val_score(clf, X, Y, cv=10))



