from myfunctions import *

samples_dir = "/home/theodore/Documents/ECE/diplomatiki/NAO/snapshots12/green/"

# get features for all samples
X, Y, img_num = get_train_features(samples_dir)

select_features = [0, 3]
Xr = X[:, select_features]

# train
gamma_o = 1e-3
C_o = 1
clf = svm.SVC(kernel="rbf", gamma=gamma_o, C=C_o, probability=True)
clf.fit(Xr, Y)

n0 = X[np.where(Y == 0), 0][0].shape[0]
n50 = X[np.where(Y == 50), 0][0].shape[0]
n100 = X[np.where(Y == 100), 0][0].shape[0]

p0 = 1.0 * n0 / (n0 + n50 + n100)
p50 = 1.0 * n50 / (n0 + n50 + n100)
p100 = 1.0 * n100 / (n0 + n50 + n100)

print p0, p50, p100

# plot gaussians
fig, ax = plt.subplots(2, 3)

f_arr = [0, 1, 2, 3, 4]
xlabels = ["$x_c$", "$y_c$", "orientation", "width", "height"]
pos = [[0, 0], [0, 1], [1, 0], [1, 1], [1, 2]]

for i in range(len(f_arr)):
    f = f_arr[i]

    X0 = X[np.where(Y == 0), f][0]
    X50 = X[np.where(Y == 50), f][0]
    X100 = X[np.where(Y == 100), f][0]

    m0 = np.mean(X0)
    std0 = np.std(X0)

    m50 = np.mean(X50)
    std50 = np.std(X50)

    m100 = np.mean(X100)
    std100 = np.std(X100)

    t = np.linspace(np.min(np.concatenate((X0, X50, X100))), np.max(np.concatenate((X0, X50, X100))), 100)
    line100 = ax[pos[i][0], pos[i][1]].plot(t, p100*1/(std100 * np.sqrt(2 * np.pi)) *np.exp( - (t - m100)**2 / (2 * std100**2) ),
              linewidth=2, color='k', label='good handover')

    line50 = ax[pos[i][0], pos[i][1]].plot(t, p50*1/(std50 * np.sqrt(2 * np.pi)) *np.exp( - (t - m50)**2 / (2 * std50**2) ),
              linewidth=2, color='k', linestyle='--', label='average handover')

    line0 = ax[pos[i][0], pos[i][1]].plot(t, p0*1/(std0 * np.sqrt(2 * np.pi)) *np.exp( - (t - m0)**2 / (2 * std0**2) ),
              linewidth=2, color='k', linestyle=':', label='bad handover')

    ax[pos[i][0], pos[i][1]].set_xlabel(xlabels[i])
    ax[pos[i][0], pos[i][1]].get_yaxis().set_visible(False)

ax[0, 2].axis("off")

# plt.legend()
plt.tight_layout()
plt.show()

