from myfunctions import *

samples_dir = "/home/theodore/Documents/ECE/diplomatiki/NAO/snapshots12/green/"

frame = cv2.imread(samples_dir + "green_245_0.jpg")
found, contour1, thresh1, contours1 = get_object_contour2(frame, "green")

thresh1 = np.dstack([thresh1] * 3)

cx, cy, orientation, width, height = contour_features(contour1)
box1 = contour2box(contour1)
cv2.drawContours(thresh1, [box1], 0, (0, 255, 255), 2)
# cv2.drawContours(thresh, [box], 0, (255, 255, 255), 2)

# bbox score
real_area = cv2.contourArea(contour1)
rect_area = width * height
score_area1 = real_area / rect_area
print "score_area1 %f" % score_area1

# noise score
Nc = len(contours1)
areas_sum = 0
for i in range(Nc):
    areas_sum += cv2.contourArea(contours1[i])

score_noise1 = real_area / areas_sum
print "score_noise1 %f" % score_noise1

w = 0.3
print w * score_area1 + (1 - w) * score_noise1

# cv2.imshow("Frame", np.concatenate((frame, thresh1), axis=1))
# cv2.waitKey(0)

# ################## CONVEX HULL ###################
found, contour2, thresh2, contours2 = get_convex_hull_contour(frame, "green")

thresh2 = np.dstack([thresh2] * 3)

cx, cy, orientation, width, height = contour_features(contour2)
box2 = contour2box(contour2)
cv2.drawContours(thresh2, [box2], 0, (0, 255, 255), 2)
# cv2.drawContours(thresh, [box], 0, (255, 255, 255), 2)

# bbox score
real_area = cv2.contourArea(contour2)
rect_area = width * height
score_area2 = real_area / rect_area
print "score_area2 %f" % score_area2

# noise score
Nc = len(contours1)
areas_sum = 0
for i in range(Nc):
    areas_sum += cv2.contourArea(contours2[i])

score_noise2 = real_area / areas_sum
print "score_noise2 %f" % score_noise2

w = 0.3
print w * score_area2 + (1 - w) * score_noise2

res = np.concatenate((frame, thresh1, thresh2), axis=1)
cv2.imshow("Frame", res)
cv2.waitKey(0)

cv2.imwrite("convex_hull_result.jpg", res)


