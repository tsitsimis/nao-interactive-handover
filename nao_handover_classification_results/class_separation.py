from myfunctions import *

samples_dir = "/home/theodore/Documents/ECE/diplomatiki/NAO/snapshots12/green/"

# get features for all samples
X, Y, img_num = get_train_features(samples_dir)

# train
gamma_o = 1e-3
C_o = 1
clf = svm.SVC(kernel="rbf", gamma=gamma_o, C=C_o, probability=True)
# print np.mean(model_selection.cross_val_score(clf, X, Y, cv=10))
clf.fit(X, Y)

X0 = X[np.where(Y == 0), :][0]
X50 = X[np.where(Y == 50), :][0]
X100 = X[np.where(Y == 100), :][0]

n0 = X0.shape[0]
n50 = X50.shape[0]
n100 = X100.shape[0]

m0 = np.mean(X0, axis=0)
cov0 = np.cov(X0, rowvar=False)

m50 = np.mean(X50)
cov50 = np.cov(X50, rowvar=False)

m100 = np.mean(X100)
cov100 = np.cov(X100, rowvar=False)

Sw = cov0 + cov50 + cov100

m = (n0 * m0 + n50 * m50 + n100 * m100) / (n0 + n50 + n100)
Sb = n0*np.dot((m0 - m).T, (m0 - m)) #+ n50*np.dot((m50 - m), (m50 - m).T) + n100*np.dot((m100 - m), (m100 - m).T)

St = Sw + Sb
print np.transpose(m0 - m)




