from myfunctions import *

samples_dir = "/home/theodore/Documents/ECE/diplomatiki/NAO/snapshots10_2classes/green/"

# get features for all samples
X, Y, img_num = get_train_features(samples_dir)
cv = 10
accuracy = np.zeros(cv)
conf = np.zeros((2, 2))

for i in range(cv):
    # split train/test
    X_train, X_test, Y_train, Y_test = model_selection.train_test_split(X, Y, test_size=0.3)

    # train
    gamma_o = 0.0001
    C_o = 10
    clf_svm = svm.SVC(kernel="rbf", gamma=gamma_o, C=C_o, probability=True)
    clf_svm.fit(X_train, Y_train)

    # test
    Y_pred = clf_svm.predict(X_test)
    accuracy[i] = metrics.accuracy_score(Y_test, Y_pred)
    c = metrics.confusion_matrix(Y_test, Y_pred, labels=[0, 100])
    c = 1.0 * c / c.sum(axis=1)[:, np.newaxis]
    c *= 100
    c = np.around(c, 0)
    conf += c

print "accuracy: %f" % np.mean(accuracy)
conf /= cv
# print conf

plt.figure()
plot_confusion_matrix(conf, classes=["bad", "good"],
                      title='Confusion matrix')
plt.show()

