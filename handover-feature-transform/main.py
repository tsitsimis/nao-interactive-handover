from myfunctions import *

dir1 = "/home/theodore/Documents/ECE/diplomatiki/NAO/snapshots8/green/"

X, Y, img_num1 = get_train_features(dir1)      # get features for all images
c = np.mean(X[np.where(Y == 100), :], axis=1)  # best grasping

# train classifier
clf = svm.SVC(kernel='rbf', gamma=0.0001, C=10, probability=True)
acc = np.mean(model_selection.cross_val_score(clf, X, Y, cv=10))
clf.fit(X, Y)

# connect to NAO
IP = "192.168.0.108"
# IP = "172.17.0.1"
PORT = 9559
motion_proxy = ALProxy("ALMotion", IP, PORT)
video_proxy = ALProxy("ALVideoDevice", IP, PORT)
posture_proxy = ALProxy("ALRobotPosture", IP, PORT)

# initialize camera
subscriber = init_cam(video_proxy)

# initialize joints
speed = 0.2
nao_stiffness(IP, PORT, 1.0)
nao_posture(IP, PORT, "Crouch")
init_arm(motion_proxy, 0.5)
init_head(motion_proxy)
motion_proxy.setAngles(R_HAND, HAND_OPEN, 0.2)  # open hand

time.sleep(3)
Z0 = get_arm_depth(motion_proxy)

# store object's last 5 positions
history = Queue.Queue(maxsize=5)
grasped_it = False
cnt_grasp = 0
cnt_bad_dim = 0
move_arm = True
x0_d = 0
x1_d = 0

y = np.zeros(5)

while True:
    frame = cam2numpy(video_proxy, subscriber)                    # read image from NAO camera
    found, contour, thresh = get_object_contour(frame, "green")  # object contour

    if grasped_it and (not found):
        grasped_it = False
        motion_proxy.setAngles(R_HAND, 1.0, 0.2)

    if found:
        box = contour2box(contour)  # contour bounding box
        features = cx, cy, orientation, width, height = contour_features(contour)  # contour features
        features = np.array(features)
        history, is_steady, sx, sy = is_object_steady(history, cx, cy)  # child holds the object steady?

        x = features
        x = np.reshape(x, (1, -1))

        # transform features
        x[0][0] += x0_d
        x[0][1] -= x1_d

        if grasped_it:
            Y_pred = clf.predict(x)
            y[cnt_grasp] = Y_pred
            cnt_grasp += 1
            if cnt_grasp >= 5:
                if np.sum(y) < 4 * 100:
                    grasped_it = False
                    cnt_grasp = 0
                    y = np.zeros(5)
                    motion_proxy.setAngles(R_HAND, 1.0, 0.2)
                else:
                    break

        is_steady = True
        if is_steady:
            Y_pred = clf.predict(x)  # predict grasping quality

            bad_dim = np.argmax(np.abs(x - c))
            print nao_response(bad_dim)
            if (bad_dim == 0) and move_arm:
                cnt_bad_dim += 1
                if cnt_bad_dim >= 5:
                    delta_u = c[0][0] - x[0][0]
                    # delta_v = c[0][1] - x[0][1]
                    # delta_x = 0.0001 * delta_u
                    # delta_y = 0.0001 * delta_v
                    delta_x = 0.02
                    delta_y = 0.0
                    # nao_move_arm(motion_proxy, delta_x, delta_y)  # SIGNS!!!

                    x_old = motion_proxy.getPosition(R_ARM, motion.FRAME_TORSO, False)[1]
                    motion_proxy.setAngles(R_SHOULDER_ROLL, 17 * almath.TO_RAD, 0.03)
                    time.sleep(3)
                    x_new = motion_proxy.getPosition(R_ARM, motion.FRAME_TORSO, False)[1]
                    delta_x = x_new - x_old

                    x0_d = fx * delta_x / Z0
                    x1_d = fy * delta_y / Z0
                    print x0_d, x1_d
                    print np.int(c[0][0] - x0_d), np.int(c[0][1] + x1_d)
                    cnt_bad_dim = 0
                    move_arm = False

            if Y_pred == 0:                 # bad handover
                color = (0, 0, 255)
            elif Y_pred == 50:              # average handover
                color = (255, 255, 255)
            elif Y_pred == 100:             # good handover
                color = (255, 0, 0)
                motion_proxy.setAngles(R_HAND, 0.0, 0.2)
                grasped_it = True

        cv2.drawContours(frame, [box], 0, color, 2)  # draw detected object bounding box
        cv2.circle(frame, (cx, cy), 5, color, thickness=3)

    cv2.circle(frame, (np.int(c[0][0]), np.int(c[0][1])), 8, (255, 0, 0), thickness=2)
    cv2.circle(frame, (np.int(c[0][0] - x0_d), np.int(c[0][1] + x1_d)), 5, (0, 255, 0), thickness=3)
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)  # show frame
    cv2.imshow("Frame", frame)
    # cv2.imshow("Frame", np.concatenate((frame, np.dstack([thresh] * 3)), axis=1))

    key = cv2.waitKey(1) & 0xFF  # press q to exit
    if key == ord("q"):
        break

video_proxy.unsubscribe(subscriber)


