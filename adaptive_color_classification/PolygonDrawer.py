import cv2
import numpy as np

class PolygonDrawer:
    def __init__(self, img):
        self.img_window = "img"
        self.img = img
        self.mask = np.zeros(np.shape(img)[0:2])
        self.done = False
        self.current = (0, 0)
        self.points = []
        self.line_color = (255, 255, 255)

    def on_mouse(self, event, x, y, buttons, user_param):
        if self.done:
            return

        if event == cv2.EVENT_MOUSEMOVE:
            self.current = (x, y)
        elif event == cv2.EVENT_LBUTTONDOWN:
            self.points.append((x, y))
        elif event == cv2.EVENT_RBUTTONDOWN:
            self.done = True

    def get_mask(self):
        cv2.namedWindow(self.img_window)
        cv2.imshow(self.img_window, self.img)
        cv2.waitKey(1)
        cv2.setMouseCallback(self.img_window, self.on_mouse)

        while not self.done:
            if len(self.points) > 0:
                cv2.polylines(self.img, np.array([self.points]), False, self.line_color, 2)
                cv2.polylines(self.mask, np.array([self.points]), False, self.line_color, 2)
            cv2.imshow(self.img_window, self.img)
            if cv2.waitKey(50) == 27: # ESC hit
                self.done = True

        if len(self.points) > 0:
            cv2.fillPoly(self.img, np.array([self.points]), self.line_color)
            cv2.fillPoly(self.mask, np.array([self.points]), self.line_color)

        cv2.imshow(self.img_window, self.img)
        cv2.waitKey()
        cv2.destroyWindow(self.img_window)
        return self.mask