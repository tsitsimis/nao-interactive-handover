import cv2
import numpy as np
from sklearn.mixture import GaussianMixture
from scipy.stats import multivariate_normal
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from imutils import build_montages
from my_utils import *
import time

# train
# color_points = collect_samples(1, save=True, filename="lab_dark.txt")
color_points = load_samples("samples_lab.txt")
mean, cov = fit_model(color_points, 2)

# test live image
camera = cv2.VideoCapture(0)
while True:
    # grab the current frame
    (grabbed, frame) = camera.read()
    frame = np.flip(frame, axis=1)

    # probability image
    proba_img = eval_sample(frame, mean, cov)

    # threshold image
    mask = get_contour(proba_img, t=10)

    # show results
    show_results(frame, proba_img, mask)

    key = cv2.waitKey(1) & 0xFF
    if key == ord("q"):
        img = frame
        break

camera.release()
cv2.destroyAllWindows()
